package com.xjt.excel.mapper;

import com.xjt.excel.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface UserMapper extends BaseMapper<User> {

    Integer SavaData(String savetableName,String tableName, List excelColumnList, List colCodeList);
}
