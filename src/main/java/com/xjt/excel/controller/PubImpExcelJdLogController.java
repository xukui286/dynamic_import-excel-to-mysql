package com.xjt.excel.controller;


import com.xjt.excel.service.IPubImpExcelJdLogService;
import com.xjt.excel.util.BaseControllerUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <p>
 * 批量导入日志信息表（提供导入日志查询功能） 前端控制器
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@RestController
@RequestMapping("/pub-imp-excel-jd-log")
@AllArgsConstructor
public class PubImpExcelJdLogController {

    IPubImpExcelJdLogService iPubImpExcelJdLogService;
    /**
     *
     * @param file      上传的excel文件
     * @param menuId        配置表的对应id
     * @param response      响应体对象
     */
    @RequestMapping(value = "/uploads",method = RequestMethod.POST)
    @ResponseBody
    public void importExcel(@RequestParam("file") MultipartFile file, Long menuId, HttpServletResponse response) {
        Map<String, Object> map = iPubImpExcelJdLogService.importExcel(file, menuId);
        BaseControllerUtil.renderResult(response,map);
    }
}
