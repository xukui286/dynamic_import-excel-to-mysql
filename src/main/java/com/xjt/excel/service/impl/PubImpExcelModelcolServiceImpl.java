package com.xjt.excel.service.impl;

import com.xjt.excel.entity.PubImpExcelModelcol;
import com.xjt.excel.mapper.PubImpExcelModelcolMapper;
import com.xjt.excel.service.IPubImpExcelModelcolService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 批量导入数据字段对应配置信息表 服务实现类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Service
public class PubImpExcelModelcolServiceImpl extends ServiceImpl<PubImpExcelModelcolMapper, PubImpExcelModelcol> implements IPubImpExcelModelcolService {

}
