package com.xjt.excel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xjt.excel.entity.PubImpExcelModel;
import com.xjt.excel.entity.PubImpExcelModelcol;

import java.util.List;

/**
 * <p>
 * 批量导入配置信息表 服务类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface IPubImpExcelModelService extends IService<PubImpExcelModel> {

    public void createTable(String tableName, List<PubImpExcelModelcol> list);

    public Integer saveData(String tableName, List<List<String>> excelInfo);
}
