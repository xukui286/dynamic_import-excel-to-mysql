package com.xjt.excel.service;

import java.util.List;

/**
 * @author xu
 * @Description 将临时表数据导入正式库数据需要实现的接口
 * @createTime 2021年05月13日 13:39:00
 */
public interface IImpExcelService {
    Integer importExcelToDb(String tableName, List<String> excelColumnList,List<String> colCodeList);
}
