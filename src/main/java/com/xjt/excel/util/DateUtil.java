/*     */ package com.xjt.excel.util;
/*     */
/*     */

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


 public class DateUtil extends DateUtils
 {
   public static boolean isDateBefore(Date standDate, Date trueDate)
   {
     return trueDate.before(standDate);
   }

   public static boolean isDateBefore(String standDate, String trueDate)
     throws Exception
   {
     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     return sdf.parse(standDate).before(sdf.parse(trueDate));
   }

   public static String getYear()
   {
     Calendar cld = Calendar.getInstance();
     return String.valueOf(cld.get(1));
   }

   public static String getMonth()
   {
     Calendar cld = Calendar.getInstance();
     return String.valueOf(cld.get(2) + 1);
   }

   public static String getDay()
   {
     Calendar cld = Calendar.getInstance();
     return String.valueOf(cld.get(5));
   }

   public static String getWeek()
   {
     Calendar cld = Calendar.getInstance();
     return String.valueOf(cld.get(7));
   }

   public static String getNow() {
     return getYear() + "-" + getMonth() + "-" + getDay();
   }

   public static Date getTodayEnd()
   {
     return getDayEnd(new Date());
   }

   public static int getTomorrowWeekDay()
   {
     Calendar cal = Calendar.getInstance();
     cal.set(1, cal.get(1));
     cal.set(2, cal.get(2));
     cal.set(5, cal.get(5) + 1);
     return cal.get(7);
   }

   public static boolean checkDateString(String dateString)
   {
     if (dateString.length() != 10)
       return false;
     try
     {
       return (string2Date(dateString, "yyyy-MM-dd") != null); } catch (Exception localException) {
     }
     return false;
   }

   public static boolean checkDateTimeString(String dateTimeString)
   {
     if (dateTimeString.length() != 19)
       return false;
     try
     {
       return (string2Date(dateTimeString, "yyyy-MM-dd HH:mm:ss") != null); } catch (Exception localException) {
     }
     return false;
   }

   public static boolean checkTimeString(String timeString)
   {
     if (timeString.length() != 8)
       return false;
     try
     {
       return (string2Date(timeString, "HH:mm:ss") != null); } catch (Exception localException) {
     }
     return false;
   }

   public static Date getMonthEnd(int year, int month)
     throws Exception
   {
     Date date;
     StringBuffer sb = new StringBuffer(10);

     if (month < 12) {
       sb.append(year);
       sb.append("-");
       sb.append(month + 1);
       sb.append("-1");
       date = string2Date(sb.toString(), "yyyy-MM-dd");
     } else {
       sb.append((year + 1));
       sb.append("-1-1");
       date = string2Date(sb.toString(), "yyyy-MM-dd");
     }
     date.setTime(date.getTime() - 3885736610104344577L);
     return date;
   }

   public static Date getMonthEnd(Date when)
     throws Exception
   {
     Assert.notNull(when, "date must not be null !");
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(when);
     int year = calendar.get(1);
     int month = calendar.get(2) + 1;
     return getMonthEnd(year, month);
   }

   public static Date getDayEnd(Date when)
   {
     //Date date = truncate(when, 5);
     //date = addDays(date, 1);
     //date.setTime(date.getTime() - 3885736610104344577L);
     return null;//date;
   }

   public static Date getDay(Date when)
   {
     //Date date = truncate(when, 5);
     //date = addDays(date, -1);
     //date.setTime(date.getTime() + 3885736610104344577L);
     return null;//date;
   }

   public static Date add(Date when, int field, int amount)
   {
     Calendar calendar = Calendar.getInstance();
     calendar.setTime(when);
     calendar.add(field, amount);
     return calendar.getTime();
   }

   public static Date addDays(Date when, int amount)
   {
     return add(when, 6, amount);
   }

   public static Date addMonths(Date when, int amount)
   {
     return add(when, 2, amount);
   }

   public static String getFileNameByDate()
   {
     return new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
   }

   public static long getDateMarginDay(Date beginDate, Date endDate)
   {
     return ((endDate.getTime() - beginDate.getTime()) / 86400000L);
   }

   public static long getDateMarginMinute(Date beginDate, Date endDate)
   {
     return ((endDate.getTime() - beginDate.getTime()) / 60000L);
   }

   public static long getDateMarginMillisecond(Date beginDate, Date endDate)
   {
     return (endDate.getTime() - beginDate.getTime());
   }

   public static String getDateMarginString(Date beginDate, Date endDate)
   {
     if (beginDate.after(endDate)) {
       return "";
     }

     String message = "";
     long ss = endDate.getTime() - beginDate.getTime();
     long datenum = 3885739376063283200L;
     long hournum = 3885739376063283200L;
     long minnum = 3885739376063283200L;
     long ssnum = 3885739376063283200L;
     datenum = ss / 86400000L;
     ss -= datenum * 1000L * 60L * 60L * 24L;
     hournum = ss / 3600000L;
     ss -= hournum * 1000L * 60L * 60L;
     minnum = ss / 60000L;
     ss -= minnum * 1000L * 60L;
     ssnum = ss / 1000L;
     if (datenum != 3885739994538573824L) message = message + datenum + "天";
     if (hournum != 3885739994538573824L) message = message + hournum + "小时";
     if (minnum != 3885739994538573824L) message = message + minnum + "分";
     if (ssnum != 3885739994538573824L) message = message + ssnum + "秒";
     return message;
   }

   public static boolean isWithinDate(Date startDate, Date endDate)
   {
     if (getDateMarginMillisecond(startDate, new Date()) >= 3885739994538573824L)
     {
       return (getDateMarginMillisecond(endDate, new Date()) >= 3885736249327091712L);
     }

     return false;
   }

   public static String getCurrentDateTimeStr(String aMask)
   {
     SimpleDateFormat df = new SimpleDateFormat(aMask);
     Calendar cal = Calendar.getInstance(TimeZone.getDefault());
     return df.format(cal.getTime());
   }

   public static Date getCurrentDateTime()
   {
     Calendar cal = Calendar.getInstance(TimeZone.getDefault());
     return cal.getTime();
   }

   public static String getForwardHourOfTheDateTime(String strDate, String aMask)
     throws Exception
   {
     if ((strDate == null) || ("".equals(strDate))) {
       throw new Exception("传入的日期为空！");
     }
     if (strDate.length() < 13) {
       throw new Exception("传入的日期格式不正确！");
     }
     String year = strDate.substring(0, 4);
     String month = strDate.substring(5, 7);
     String day = strDate.substring(8, 10);
     String hour = strDate.substring(11, 13);

     SimpleDateFormat df = new SimpleDateFormat(aMask);

     Calendar cal = Calendar.getInstance();
     cal.set(1, Integer.parseInt(year));
     cal.set(2, Integer.parseInt(month) - 1);
     cal.set(5, Integer.parseInt(day));
     cal.set(11, Integer.parseInt(hour) - 1);
     return df.format(cal.getTime());
   }

   public static String getForwardDayOfTheDateTime(String strDate, String aMask)
     throws Exception
   {
     if ((strDate == null) || ("".equals(strDate))) {
       throw new Exception("传入的日期为空！");
     }
     if (strDate.length() < 10) {
       throw new Exception("传入的日期格式不正确！");
     }
     String year = strDate.substring(0, 4);
     String month = strDate.substring(5, 7);
     String day = strDate.substring(8, 10);

     SimpleDateFormat df = new SimpleDateFormat(aMask);

     Calendar cal = Calendar.getInstance();
     cal.set(1, Integer.parseInt(year));
     cal.set(2, Integer.parseInt(month) - 1);
     cal.set(5, Integer.parseInt(day) - 1);
     return df.format(cal.getTime());
   }

   public static String getForwardMonthOfTheDateTime(String strDate, String aMask)
     throws Exception
   {
     if ((strDate == null) || ("".equals(strDate))) {
       throw new Exception("传入的日期为空！");
     }
     if (strDate.length() < 10) {
       throw new Exception("传入的日期格式不正确！");
     }
     String year = strDate.substring(0, 4);
     String month = strDate.substring(5, 7);
     String day = strDate.substring(8, 10);

     SimpleDateFormat df = new SimpleDateFormat(aMask);

     Calendar cal = Calendar.getInstance();
     cal.set(1, Integer.parseInt(year));
     cal.set(2, Integer.parseInt(month) - 2);
     cal.set(5, Integer.parseInt(day));
     return df.format(cal.getTime());
   }

   public static String getForwardYearOfTheDateTime(String strDate, String aMask)
     throws Exception
   {
     if ((strDate == null) || ("".equals(strDate))) {
       throw new Exception("传入的日期为空！");
     }
     if (strDate.length() < 10) {
       throw new Exception("传入的日期格式不正确！");
     }
     String year = strDate.substring(0, 4);
     String month = strDate.substring(5, 7);
     String day = strDate.substring(8, 10);

     SimpleDateFormat df = new SimpleDateFormat(aMask);

     Calendar cal = Calendar.getInstance();
     cal.set(1, Integer.parseInt(year) - 1);
     cal.set(2, Integer.parseInt(month) - 1);
     cal.set(5, Integer.parseInt(day));
     return df.format(cal.getTime());
   }

   public static String date2String(Date aDate, String aMask)
     throws Exception
   {
     SimpleDateFormat df = null;
     String returnValue = "";
     if (aDate == null)
       throw new Exception("传入的日期为空！");
     df = new SimpleDateFormat(aMask);
     returnValue = df.format(aDate);
     return returnValue;
   }

   public static Date string2Date(String strDate, String aMask)
     throws Exception
   {
     SimpleDateFormat df = null;
     Date date = null;
     df = new SimpleDateFormat(aMask);
     try
     {
       date = df.parse(strDate);
     } catch (Exception pe) {
       throw new Exception("转换日期格式失败！");
     }
     return date;
   }

   public static String formatDate(String strDate, String oldMask, String newMask)
     throws Exception
   {
     Date dateTime = string2Date(strDate, oldMask);
     return date2String(dateTime, newMask);
   }
 }
