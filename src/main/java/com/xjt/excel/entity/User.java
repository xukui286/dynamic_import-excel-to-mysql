package com.xjt.excel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEnity {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    private String state;

    private String sex;

    private Integer age;


}
