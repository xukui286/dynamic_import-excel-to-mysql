INSERT INTO `pub_imp_excel_model`(`id`, `oper_uid`, `menu_id`, `state`, `sheet_name`, `model_name`, `ind`, `service_name`, `fixed_col`) VALUES (1, 1, 1, 1, '用户表', '用户表', 0, 'userServiceImpl', 1);


INSERT INTO `pub_imp_excel_modelcol`(`id`, `model_id`, `col_code`, `excel_column`, `state`, `validate_zz`, `validate_tip`, `is_req`, `ind`, `col_name`, `field_type`) VALUES (1, 1, 'username', 'a1', 1, '[anything]', '1', 0, 1, 'test2', 'varchar(255)');
INSERT INTO `pub_imp_excel_modelcol`(`id`, `model_id`, `col_code`, `excel_column`, `state`, `validate_zz`, `validate_tip`, `is_req`, `ind`, `col_name`, `field_type`) VALUES (2, 1, 'password', 'a2', 1, '[anything]', '1', 0, 1, 'test2', 'varchar(255)');
INSERT INTO `pub_imp_excel_modelcol`(`id`, `model_id`, `col_code`, `excel_column`, `state`, `validate_zz`, `validate_tip`, `is_req`, `ind`, `col_name`, `field_type`) VALUES (3, 1, 'state', 'a3', 1, '[anything]', '1', 0, 1, 'test2', 'varchar(255)');
INSERT INTO `pub_imp_excel_modelcol`(`id`, `model_id`, `col_code`, `excel_column`, `state`, `validate_zz`, `validate_tip`, `is_req`, `ind`, `col_name`, `field_type`) VALUES (4, 1, 'sex', 'a4', 1, '[anything]', '1', 0, 1, 'test2', 'varchar(255)');
INSERT INTO `pub_imp_excel_modelcol`(`id`, `model_id`, `col_code`, `excel_column`, `state`, `validate_zz`, `validate_tip`, `is_req`, `ind`, `col_name`, `field_type`) VALUES (5, 1, 'age', 'a5', 1, '[anything]', '1', 0, 1, 'test2', 'int(11)');


INSERT INTO `pub_imp_excel_valid_rule`(`id`, `state`, `valid_expression`, `valid_tip`, `valid_code`, `remark`) VALUES (1, 1, '^[1-9]\\d*$', '请输入正整数', 'num_positive', '校验正整数');
INSERT INTO `pub_imp_excel_valid_rule`(`id`, `state`, `valid_expression`, `valid_tip`, `valid_code`, `remark`) VALUES (2, 1, '\\d{3}-\\d{8}|\\d{4}-\\{7,8}', '电话号码格式不对', 'num_phone', '校验电话号码');
INSERT INTO `pub_imp_excel_valid_rule`(`id`, `state`, `valid_expression`, `valid_tip`, `valid_code`, `remark`) VALUES (3, 1, '.*', '任意字符', 'anything', '任意字符');
INSERT INTO `pub_imp_excel_valid_rule`(`id`, `state`, `valid_expression`, `valid_tip`, `valid_code`, `remark`) VALUES (4, 1, '^\\d{5}$', '请输入五位数字', 'num_5', '数字长度不对');
